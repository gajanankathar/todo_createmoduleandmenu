{
    'name': 'To-Do Create Module Application',
    'description': 'Manage    your    personal    Tasks    with    this    module.',
    'author': 'Daniel    Reis',
    'depends': ['mail'],
    'data' : [
              'views/todo_view.xml',
              ],
    'application': True,
}